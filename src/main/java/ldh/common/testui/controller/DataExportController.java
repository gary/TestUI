package ldh.common.testui.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.cell.CheckBoxTableCell;
import ldh.common.testui.cell.ObjectStringConverter;
import ldh.common.testui.constant.ParamCategory;
import ldh.common.testui.model.ParamModel;
import ldh.common.testui.model.TreeNode;
import ldh.common.testui.util.DataUtil;
import ldh.common.testui.util.ThreadUtilFactory;
import ldh.common.testui.vo.DataExportItem;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DataExportController extends BaseController implements Initializable {

    @FXML private ChoiceBox<ParamModel> databaseChoiceBox;
    @FXML private TableView<DataExportItem> tableNamesTableView;
    @FXML private TableColumn<DataExportItem, Boolean> isSelectedTableColumn;
    @FXML private TableColumn<DataExportItem, String> tableNameTableColumn;
    @FXML private TableColumn<DataExportItem, String> whereTableColumn;

    public void setTreeItem(TreeItem<TreeNode> treeItem) {
        this.treeItem = treeItem;
        List<ParamModel> paramModels = DataUtil.getAllParamModels(treeItem, ParamCategory.Database);
        databaseChoiceBox.getItems().addAll(paramModels);

        loadData();
    }

    private void loadData() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        databaseChoiceBox.setConverter(new ObjectStringConverter<>(paramModel -> paramModel.getName()));
        databaseChoiceBox.getSelectionModel().selectedItemProperty().addListener((b, n, o)->{
            changeTableContent(n);
        });

        isSelectedTableColumn.setCellFactory(CheckBoxTableCell.forTableColumn(isSelectedTableColumn));
    }

    private void changeTableContent(ParamModel paramModel) {
        if (paramModel == null) return;
        tableNamesTableView.getItems().clear();

        loadDatabaseTableNames();
    }

    private void loadDatabaseTableNames() {
        ThreadUtilFactory.getInstance().submit(()->{
            return null;
        }, (task)->{

        });
    }
}
